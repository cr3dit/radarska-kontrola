package com.example.alen.radarskakontrola;

/**
 * Created by alen on 10.06.2017..
 */

class FiksniRadar {

    private String ulica, mjesto;
    private double lat,longtitude;
    private int kanton;
    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setKanton(int kanton) {
        this.kanton = kanton;
    }


    public void setLong(double longtitude) {
        this.longtitude = longtitude;
    }

    public void setMjesto(String mjesto) {
        this.mjesto = mjesto;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getUlica() {
        return ulica;
    }

    public String getMjesto() {
        return mjesto;
    }

    public int getKanton() {
        return kanton;
    }

    public double getLat() {
        return lat;
    }

    public double getLongtitude() {
        return longtitude;
    }
}
