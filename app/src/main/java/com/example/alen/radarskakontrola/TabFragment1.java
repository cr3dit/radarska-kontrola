package com.example.alen.radarskakontrola;

/**
 * Created by alen on 09.06.2017..
 */


import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class TabFragment1 extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback{
    MapView mMapView;
    private GoogleMap googleMap;
DatabaseFixedLocation dbHelper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_fragment_1, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button

                // For dropping a marker at a point on the Map
                LatLng bosna = new LatLng(43.9513087, 17.8885529);

                googleMap.setMyLocationEnabled(true);

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(bosna).zoom(7).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                drawMarkers();

            }
        });
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    public void drawMarkers(){
        dbHelper = new DatabaseFixedLocation(getActivity());
        List<FiksniRadar> dataCollection;

        dataCollection = dbHelper.getAllFixedRadars();
        int dataSize = dataCollection.size();
        final String[] ulica = new String[dataSize];
        final String[] mjesto = new String[dataSize];
        final int[] kanton = new int[dataSize];
        final double[] lat = new double[dataSize];
        final double[] longtitude = new double[dataSize];

        int counter = 0;
        for (FiksniRadar radari : dataCollection) {
            ulica[counter] = radari.getUlica();
            mjesto[counter]=radari.getMjesto();
            kanton[counter]=radari.getKanton();
            lat[counter]=radari.getLat();
            longtitude[counter]=radari.getLongtitude();

            googleMap.addMarker(new MarkerOptions().position(new LatLng(lat[counter],longtitude[counter])).title(mjesto[counter]).snippet(ulica[counter]).icon(BitmapDescriptorFactory.fromResource(R.drawable.iccam)));
            


            
            Log.d("ULICA", ulica[counter]);
            Log.d("MJESTO", mjesto[counter]);
            Log.d("KANTON", kanton[counter]+"");
            Log.d("LAT", lat[counter]+"");
            Log.d("LONG", longtitude[counter]+"");


            counter++;


        }



    }
}