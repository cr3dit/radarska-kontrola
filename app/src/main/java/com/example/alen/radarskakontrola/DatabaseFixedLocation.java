package com.example.alen.radarskakontrola;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alen on 10.06.2017..
 */

public class DatabaseFixedLocation extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "radarska_kontrola";


    private static final String TABLE_FIKSNI_RADARI = "fiksni_radari";

    private static final String KEY_ID = "_id";

    private static final String KEY_KANTON = "kanton";
    private static final String KEY_MJESTO = "mjesto";
    private static final String KEY_ULICA = "ulica";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LONG = "long";
    private static final String CREATE_TABLE_FIKSNI_RADARI = "CREATE TABLE " + TABLE_FIKSNI_RADARI + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_KANTON + " INTEGER," + KEY_MJESTO + " TEXT," + KEY_ULICA +
            " TEXT," + KEY_LAT + " DOUBLE," + KEY_LONG + " DOUBLE" + ")";


    public DatabaseFixedLocation(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(CREATE_TABLE_FIKSNI_RADARI);

        //USK kanton br 1

       addRadar(db, 1, "Velika Kladuša", "Ibrahima Mržljaka",45.187844, 15.802340);
       addRadar(db, 1, "Cazin", "Huremagići",44.989199, 15.896453);
       addRadar(db, 1, "Cazin", "Kličići",44.947574, 15.939250);
       addRadar(db, 1, "Cazin", "Ćuprija",44.965332, 15.922195);
       addRadar(db, 1, "Cazin", "Ostrožac",44.909064, 15.931809);
       addRadar(db, 1, "Bihać", "Dr.Irfana Ljubljankića",44.823446, 15.851636);
       addRadar(db, 1, "Bihać", "Jablanska ",44.826939, 15.881655);
       addRadar(db, 1, "Bihać", "Jablanska - Market Nely",44.825682, 15.8817139);
       addRadar(db, 1, "Bihać", "Jasika",44.862277, 15.789852);
       addRadar(db, 1, "Bihać", "Hasan paše Predojevića",44.823380, 15.854109);
       addRadar(db, 1, "Bihać", "Bedem",44.814829, 15.867806);
       addRadar(db, 1, "Bihać", "Kostela",44.881114, 15.896032);
       addRadar(db, 1, "Bihać", "Ripač",44.766974, 15.953486);
       addRadar(db, 1, "Bosanski Petrovac", "BP energopetrol",44.546376, 16.359322);
       addRadar(db, 1, "Ključ", "BP Čavkunić",44.537292, 16.763118);
       addRadar(db, 1, "Ključ", "Pudin Han",44.552353, 16.745041);
       addRadar(db, 1, "Ključ", "Velečevo",44.506711, 16.821340);
       addRadar(db, 1, "Bosanska Krupa", "Ljusina",44.940264, 16.161764);
       addRadar(db, 1, "Bosanska Krupa", "Bihaćka",44.878930, 16.139539);
       addRadar(db, 1, "Bosanska Krupa", "Radnička",44.890589, 16.154139);
       addRadar(db, 1, "Sanski Most", "Podbriježje",44.772965, 16.643116);
       addRadar(db, 1, "Sanski Most", "Tomina",44.706372, 16.721739);
       //Posavski kanton br 2
       addRadar(db, 2, "Orašje", "Tc Bingo",45.019997, 18.700138);
       //TK 3
        addRadar(db, 3, "Tuzla", "Zmaja od Bosne",44.534338, 18.680569);
       //ZD 4
       addRadar(db, 4, "Zenica", "Donja vraca",44.256722, 17.893594);
       addRadar(db, 4, "Žepče", "Brankovići",44.485147, 18.072884);
       addRadar(db, 4, "Maglaj", "Misurići",44.555479, 18.095091);
       addRadar(db, 4, "Tešanj", "Šije",44.651124, 18.076964);
       //BPK 5
       addRadar(db, 5, "Goražde", "kod Donjeg mosta",43.670348, 18.980799);
       addRadar(db, 5, "Goražde", "31. Drinske brigade",43.649339, 18.963486);
       //SBK 6
       addRadar(db, 6, "Travnik", "Kalibunar",44.229534, 17.643025);
       addRadar(db, 6, "Vitez", "Kod impregnacije",44.151178, 17.806336);
       addRadar(db, 6, "Busovaća", "Kaćuni",44.066742, 17.939347);
       addRadar(db, 6, "Kiseljak", "Brnjaci",43.913043, 18.128750);
       addRadar(db, 6, "Novi Travnik", "Nević Polje",44.197266, 17.707402);
       //HNK 7
       addRadar(db, 7, "Mostar", "Kralja Tomislava",43.347986, 17.800232);
       addRadar(db, 7, "Mostar", "Bulevar",43.334223, 17.813918);
       //ZHK 8
       addRadar(db, 8, "Ljubuški", "Studenci",43.171304, 17.610726);
       addRadar(db, 8, "Ljubuški", "Donji Radišići",43.214324, 17.519686);
       addRadar(db, 8, "Ljubuški", "Vitina",43.239859, 17.479251);
       addRadar(db, 8, "Ljubuški", "Klobuk",43.278500, 17.445898);
       addRadar(db, 8, "Grude", "Prispa",43.358391, 17.421987);
       addRadar(db, 8, "Grude", "dr. Franje Tuđmana",43.368041, 17.420901);
       addRadar(db, 8, "Grude", "Sovići",43.398231, 17.345460);
       addRadar(db, 8, "Široki Brijeg", "Dobrič",43.361733, 17.673204);
       addRadar(db, 8, "Široki Brijeg", "Knešpolje",43.366598, 17.633646);
       addRadar(db, 8, "Široki Brijeg", "Trn",43.379306, 17.545020);
       addRadar(db, 8, "Široki Brijeg", "Kočerin",43.389842, 17.484794);
       addRadar(db, 8, "Posušje", "Rastovača",43.464336, 17.342631);
       //sarajevo 9
       addRadar(db, 9, "Sarajevo", "Zmaja od Bosne",43.854023, 18.392495);
       addRadar(db, 9, "Sarajevo", "Zmaja od Bosne, Kampus",43.855648, 18.397720);
       addRadar(db, 9, "Sarajevo", "Bulevar Meše Selimovića",43.847365, 18.353921);
       addRadar(db, 9, "Sarajevo", "Džemala Bijedića",43.842740, 18.328670);
       addRadar(db, 9, "Sarajevo", "Otoka",43.849119, 18.366814);
       addRadar(db, 9, "Sarajevo", "A1, Malešići",43.936760, 18.294588);
       addRadar(db, 9, "Sarajevo", "Binježevo",43.844102, 18.221240);
       addRadar(db, 9, "Sarajevo", "Dobrinja, Kurta Schorka",43.822589, 18.350676);
       //10
       addRadar(db, 10, "Drvar", "Jole Marića",44.368475, 16.371821);
       addRadar(db, 10, "Livno", "Suhača",43.839327, 16.975038);
       addRadar(db, 10, "Livno", "Gorička",43.811834, 17.040776);
       addRadar(db, 10, "Livno", "M-16",43.823698, 16.984719);
       addRadar(db, 10, "Kupres", "M-16",43.981637, 17.271276);
       addRadar(db, 10, "Tomislavgrad", "Blažuj",43.739332, 17.224263);
       addRadar(db, 10, "Tomislavgrad", "Kolo",43.701268, 17.217954);
       
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FIKSNI_RADARI);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FIKSNI_RADARI);
        onCreate(db);
    }

    public void addRadar(SQLiteDatabase db, int kanton, String mjesto, String ulica, double lat, double longtitude) {
        ContentValues values = new ContentValues();
        values.put(KEY_KANTON, kanton);
        values.put(KEY_MJESTO, mjesto);
        values.put(KEY_ULICA, ulica);
        values.put(KEY_LAT, lat);
        values.put(KEY_LONG, longtitude);

        // Inserting Row
        db.insert(TABLE_FIKSNI_RADARI, null, values);
    }

    public List<FiksniRadar> getAllFixedRadars() {
        List<FiksniRadar> fiksniRadar = new ArrayList<FiksniRadar>();
        String selectQuery = "SELECT  * FROM " + TABLE_FIKSNI_RADARI;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                FiksniRadar radar = new FiksniRadar();
                radar.setKanton(c.getInt((c.getColumnIndex(KEY_KANTON))));
                radar.setLat((c.getDouble(c.getColumnIndex(KEY_LAT))));
                radar.setLong((c.getDouble(c.getColumnIndex(KEY_LONG))));
                radar.setMjesto((c.getString(c.getColumnIndex(KEY_MJESTO))));
                radar.setUlica((c.getString(c.getColumnIndex(KEY_ULICA))));


                fiksniRadar.add(radar);

            } while (c.moveToNext());
        }
        db.close();

        return fiksniRadar;
    }
}
